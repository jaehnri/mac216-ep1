global _start

extern main                 ; The main routine is extern to this program

section .text

_start:
    xor ebp, ebp            ; Clear the base pointer to mark the outermost frame as suggested by ABI

    pop esi                 ; ESI will receive argc
    mov ecx, esp            ; ESP is now pointing at argv, so we put it in ECX

    and esp, 0xfffffff0     ; Align the stack pointer so we can support SIMD/SSE instructions,
                            ; i.e., round the stack pointer down to the nearest multiple of 16.

    push eax                ; Here, we push eax (garbage) 6 times because 2 things are going
    push eax                ; to be pushed on the stack and 8 are needed to keep the 16-byte alignment.
    push eax
    push eax
    push eax
    push eax
    push ecx                ; Stack the argv
    push esi                ; Stack the argc

    call main

    mov eax, 1              ; 1 is the identifier for sys_exit
    int 0x80                ; Send the sys_exit to the kernel