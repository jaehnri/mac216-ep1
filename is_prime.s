GLOBAL is_prime

SECTION .text

is_prime:

    ; *** Standard subroutine prologue ***
    .base_pointer_maintenance:
    	push ebp
        mov ebp, esp

        push ebx
        push ecx
        push edx

    ; *** Subroutine body ***
        mov ecx, [ebp+8]            ; Save number from argv in ecx register

    .number_less_or_equal_one:
        cmp ecx, 1
        je  .not_prime

    .number_is_two:
        cmp ecx, 2
        je .prime

    .number_is_even:
        mov edx, ecx                ; Saves ECX in EDX to make bitshift with ECX
        shr ecx, 1                  ; Bitshifts to the right, if there is carry, then it is not even
        jc  .iterate_division
        jmp .not_prime

    .iterate_division:
        mov ecx, edx                ; Recover ECX from shr instruction

        mov ebx, 3                  ; Iterator will start at 3

    .loop:
        mov eax, ebx                ; We want to stop the iteration at root(n), so we move the iterator to eax,
        mul eax                     ; Find the power of 2 of the iterator
        cmp eax, ecx                ; And check if it is greater than n itself
        jg  .prime                  ; If it is greater, then there are no divisors apart from 1

        mov eax, ecx                ; Put n in eax to divide eax by ebx,

        div ebx                     ; Note that the remainder of EAX/EBX is stored in edx

        cmp edx, 0
        je  .not_prime              ; If remainder is zero, we found a divisor

        add ebx, 2                  ; Iterator skips even numbers as we already did .number_is_even
        jmp .loop

    .not_prime:
        mov eax, 0
        jmp .end

    .prime:
        mov eax, 1

    ; *** Standard subroutine prologue ***
    .end:
        pop edx           ; Deallocate local variables
        pop ecx
        pop ebx
        mov esp, ebp      ; Put the stack pointer back at where it was before this subroutine
        pop ebp

        ret
