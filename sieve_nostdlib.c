extern void print_asm(int length, char *string);
extern int is_prime(int n);

int atoi(char* str) {
    int i, result;
    result = 0;

    for (i = 0; str[i] != '\0'; i++) {
        result = result * 10 + str[i] - '0';
    }

    return result;
}

int main(int argc, char **argv) {
    if (argc != 2) {
        print_asm(87, "[ERROR]: The only parameter should be an integer number.\nTry: ./sieve_nostdlib <NUMBER>");
        return 1;
    }

    if (is_prime(atoi(argv[1]))) {
        print_asm(2, "1\n");
    } else {
        print_asm(2, "0\n");
    }

    return 0;
}
