C_COMPILER=gcc
C_COMPILER_FLAGS=-m32 -Wall
ASSEMBLY_COMPILER=nasm
ASSEMBLY_COMPILER_FLAGS=-Wall -f elf32

### Build all ###
install: sieve sieve-asm sieve-nostdlib

### Util ###
cls:
	clear

clean: cls
	rm -f is_prime.o print.o _start.o sieve sieve_asm sieve_nostdlib

### Assembly ###
is-prime:
	$(ASSEMBLY_COMPILER) $(ASSEMBLY_COMPILER_FLAGS) is_prime.s -o is_prime.o

print-asm:
	$(ASSEMBLY_COMPILER) $(ASSEMBLY_COMPILER_FLAGS) print.s -o print.o

start:
	$(ASSEMBLY_COMPILER) $(ASSEMBLY_COMPILER_FLAGS) _start.s -o _start.o

### C ###
sieve:
	$(C_COMPILER) $(C_COMPILER_FLAGS) sieve.c -o sieve

sieve-asm: is-prime
	$(C_COMPILER) $(C_COMPILER_FLAGS) is_prime.o sieve_asm.c -o sieve_asm

sieve-nostdlib: is-prime print-asm start
	$(C_COMPILER) $(C_COMPILER_FLAGS) -nostdlib is_prime.o print.o _start.o sieve_nostdlib.c -o sieve_nostdlib