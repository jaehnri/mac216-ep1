#include <stdio.h>
#include <stdlib.h>

int is_prime(int n) {
    int counter;

    if (n <= 1 || (n % 2 == 0 && n > 2)) {
        return 0;
    }

    for (counter = 3; counter < n/2; counter+=2) {
        if (n % counter == 0) {
            return 0;
        }
    }
    return 1;
}

int main(int argc, char **argv) {
    if (argc != 2) {
        printf("[ERROR]: The only parameter should be an integer number. \nTry: ./sieve_asm <NUMBER>\n");
        exit(EXIT_FAILURE);
    }

    printf("%d\n", is_prime(atoi(argv[1])));
    exit(EXIT_SUCCESS);
}
