global print_asm

section .text

print_asm:

    ; *** Standard subroutine prologue ***
    .save_stack_state:
        push ebp                    ; Save stack state before allocate local variables
        mov  ebp, esp               ; We want to access the function parameters in the ESP using the EBP
        push eax                    ; Save general purpose registers state
        push ebx
        push ecx
        push edx

    ; *** Subroutine body ***
    .get_parameters_from_stack:
        mov edx, [ebp+8]            ; Get the message length parameter from parameters
        mov ecx, [ebp+12]           ; Get the pointer to message to write from parameters

    .write_to_stdout:
        mov ebx, 1                  ; STDOUT file descriptor identifier is 1
        mov eax, 4                  ; 4 is the sys_write identifier
        int 0x80                    ; Call kernel

    ; *** Standard subroutine epilogue ***
    .base_pointer_maintenance:
        pop edx                     ; Recover general purpose register values
        pop ecx
        pop ebx
        pop eax
        mov esp, ebp                ; Deallocate local variables
        pop ebp                     ; Restore the caller's base pointer value
        ret
