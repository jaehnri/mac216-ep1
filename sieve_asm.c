#include <stdio.h>
#include <stdlib.h>

extern int is_prime(int n);

int main(int argc, char **argv) {
    if (argc != 2) {
        printf("[ERROR]: The only parameter should be an integer number. \nTry: ./sieve_asm <NUMBER>\n");
        exit(EXIT_FAILURE);
    }

    printf("%d\n", is_prime(atoi(argv[1])));
    exit(EXIT_SUCCESS);
}
